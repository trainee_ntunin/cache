#include "stdafx.h"
#include "Cache.h"

int getLowBitsCount(int adress)
{
	int lowBitsCount;
	{
		double unround = log(GROUP_COUNT)/log(2);
		lowBitsCount = (int)unround;
		if(unround - lowBitsCount != 0) lowBitsCount++;
	}
	return lowBitsCount;
}
int getTag(int adress)
{
	return adress>>getLowBitsCount(adress);
}
int getGroupNumber(int adress)

{
	int mask = 0;
	int lowBitsCount = getLowBitsCount(adress);
	for(int i = 0; i < lowBitsCount; i++)
	{
		mask = mask << 1;
		mask++;
	}
	return adress & mask;
}

Cache::Cache()
{
	for(int i = 0; i < GROUP_COUNT; i++)
		for(int j = 0; j < GROUP_SIZE; j++)
			{
				reflection[i][j].tag = -1;
				reflection[i][j].bits = (char)0;
			}
		cacheMissCount = 0;
		mutex = CreateMutex(NULL, FALSE, NULL);
}

int Cache::shoot(int adress, int groupNumber)
{
	int tag = getTag(adress);
	for(int i = 0; i < GROUP_SIZE; i++)
		if(reflection[groupNumber][i].tag == tag) return i;
	return -1;
}
int Cache::getFreeRecord(int groupNumber)
{
	for(int i = 0; i < GROUP_SIZE; i++)
	{
		if(reflection[groupNumber][i].tag < 0) return i;
	}
	return -1;
}
bool Cache::getBit(char trueBit, int groupNumber, int recordNumber)
{
	bool result = (reflection[groupNumber][recordNumber].bits | trueBit) == trueBit;
	return result;
}
void Cache::setBit(char trueBit, int groupNumber, int recordNumber, bool value)
{	
	if(value) 
	{ 
		reflection[groupNumber][recordNumber].bits |= trueBit;
		return;
	}
	char mask = (char)255 - (trueBit-1);
	reflection[groupNumber][recordNumber].bits &= mask;
}
void Cache::refreshRBit()
{ 
	WaitForSingleObject( 
            mutex,    // handle to mutex
            INFINITE);
	for(int i = 0; i < GROUP_COUNT; i++)
		for(int j = 0; j < GROUP_SIZE; j++)
			setBit(R, i, j, false);
	ReleaseMutex(mutex);
}
int Cache::getReplaceRecordNumber(int groupNumber)
{
	int minRecordClass = 4;
	int numberRecordWithminClass;
	int recordClass;

	for(int i = 0; i < GROUP_SIZE; i++)
	{
		recordClass = (int)reflection[groupNumber][i].bits;
		if(recordClass < minRecordClass)
		{
			minRecordClass = recordClass;
			numberRecordWithminClass = i;
		}
	}
	return numberRecordWithminClass;
}
void Cache::according(int groupNumber, int recordNumber, int adress, char data)
{
	int tag = getTag(adress);
	reflection[groupNumber][recordNumber].tag = tag;
	setBit(M, groupNumber, recordNumber, true);
	setBit(R, groupNumber, recordNumber, true);
	reflection[groupNumber][recordNumber].data = data;
}
char Cache::readAccess(int adress, GeneralMemory *memory)
{
	return push(adress, memory);
}
char Cache::noncountedPush(int adress, GeneralMemory *memory)
{
	if(adress < 0) return (char)0;
	int tmp = cacheMissCount;
	char c = push(adress, memory);
	cacheMissCount = tmp;
	return c;
}
char Cache::push(int adress, GeneralMemory *memory)
{	 
	WaitForSingleObject( 
            mutex,    // handle to mutex
            INFINITE);
	if(adress < 0) return (char)0;
	int groupNumber = getGroupNumber(adress);
	int recordNumber = shoot(adress, groupNumber);
	if(recordNumber >= 0)
	{  
		setBit(R, groupNumber, recordNumber, true);
		return reflection[groupNumber][recordNumber].data; //hit
	}
	
	recordNumber = getFreeRecord(groupNumber); //miss
	if(recordNumber < 0) 
		recordNumber = getReplaceRecordNumber(groupNumber);

	setBit(R, groupNumber, recordNumber, true);
	char data = memory->getData(adress);
	if(getBit(M, groupNumber, recordNumber))
	//if(reflection[groupNumber][recordNumber].mod == 'M')	
	{
			memory->setData(adress, reflection[groupNumber][recordNumber].data);
			setBit(M, groupNumber, recordNumber, false);
			//reflection[groupNumber][recordNumber].mod = 'C';
	}

	reflection[groupNumber][recordNumber].data = data;
	reflection[groupNumber][recordNumber].tag = getTag(adress);
	cacheMissCount++;
	ReleaseMutex(mutex);
	return data;
}
void Cache::writeAccess(int adress, char data, GeneralMemory *memory)
{
	int groupNumber = getGroupNumber(adress);
	int recordNumber = shoot(adress, groupNumber);
	if(recordNumber >= 0)  
	{
		according(groupNumber, recordNumber, adress, data);
		return; // hit
	}
	//miss 
	memory->setData(adress, data);
	cacheMissCount++;
}
int Cache::getMissCount()
{
	return cacheMissCount;
}

Cache::~Cache(void)
{
	CloseHandle(mutex);
}
