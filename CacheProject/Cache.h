#pragma once
#include "stdafx.h"
#include "GeneralMemory.h"
#define R (char)3
#define M (char)1
using namespace std;

class Cache
{
	struct Record
	{
		int tag;
		char data;
		char bits;
	};

	Record reflection[GROUP_COUNT][GROUP_SIZE];
	HANDLE mutex;

	int cacheMissCount;
	int getFreeRecord(int groupNumber);
	int getReplaceRecordNumber(int groupNumber);
	int shoot(int adress, int groupNumber);
	void according(int groupNumber, int recordNumber, int adress, char data);
	bool getBit(char trueBit, int groupNumber, int recordNumber);
	void setBit(char trueBit,int groupNumber, int recordNumber, bool value);
public:
	Cache(void);
	char readAccess(int adress, GeneralMemory *memory);
	void writeAccess(int adress, char data, GeneralMemory *memory);
	void refreshRBit();
	int getMissCount();
	char push(int adress, GeneralMemory *memory);
	char noncountedPush(int adress, GeneralMemory *memory);
	~Cache(void);


};

