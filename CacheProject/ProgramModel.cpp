#include "stdafx.h"
#include "ProgramModel.h"

int getSomeValue(int value)
{
	double delta = (rand()%(value*2) - value)*_FLUCT/100;
	return int(value + delta);
}

ProgramModel::ProgramModel(MemoryController *controller)
{
	if (controller == NULL) throw invalid_argument("there is not controller");
	this->controller = controller;
	main = new Function(this, 0);
	execute();
}
void ProgramModel::Function::execute(MemoryController *controller)
{
	execute(code[0], codeSize, controller);
}
void ProgramModel::Function::execute(int start, int count, MemoryController *controller)
{
	int pointer = start;
	while(pointer < start + codeSize)
	{
		Sleep(10);
		if(start < 0 || start  + codeSize > MEMORY_SIZE) return;
		for(int i = 0; i < ciclesCount; i++)
			if(pointer == cicles[i].start) 
			{
				int repeatsCount = rand()%5;
				for(int i = 0; i < repeatsCount; i++)
					execute(cicles[i].start+1, cicles[i].countBytes, controller);
			}
		int variant = getVariant();
		switch(variant)
		{
		case _VAR:
			{
				int varNumber = rand()%varsCount;
				if(rand()%100 < 50) controller->getByte(variables[varNumber]);
				else controller->setByte(variables[varNumber], (char)('A' + rand()%27));
			}
			break;
		case _FUNCTION: 
			{
				if(functionsCount == 0) {
					controller->getByte(pointer++);
					continue;
				}
				int number = rand()%functionsCount;
				functions[number]->execute(controller);
			}
		case _COMMAND: 
			controller->getByte(pointer++);
			break;
		case _OBJECT:
			{
				int objectNumber = rand()%structuresCount;
				for(int i = 0; i < structures[objectNumber].countBytes; i++)
				{
					if(rand()%100 < 50) controller->getByte(structures[objectNumber].start + i);
					else controller->setByte(structures[objectNumber].start + i, (char)('A' + rand()%27));
				}
			}
			break;
		case _ARRAY:
			{
				int step=1;
				int arrayNumber = rand()%arraysCount;
				int start = arrays[arrayNumber].start;
				for(int i = start; i >= 0 && i < arrays[arrayNumber].countBytes; i+=step)
					if(rand()%100 < 50) controller->getByte(arrays[arrayNumber].start + i);
					else controller->setByte(arrays[arrayNumber].start + i, (char)('A' + rand()%27));

			}			
			break;
		}
		pointer++;
	}
}
void ProgramModel::execute()
{
		//int functuonsNumber = rand()%functionsCount;
		main->execute(controller);
		cout<<controller->getStatistics()*100<<"%"<<endl;
}

ProgramModel::Compound *ProgramModel::placeCompound(int size)
{
	Compound *result = new Compound;
	int start = controller->reserveBytes(size);
	if(start < 0) return NULL;
	result->start = start;
	result->countBytes = size;
	return result;
}

void ProgramModel::placeBytes(int *bytes, int &varCount, int elementsType)
{
	varCount = getSomeValue(counts[elementsType]);
	int start = controller->reserveBytes(varCount);
	if (start < 0) varCount = 0;
	for(int i = 0; i < varCount; i++)
		bytes[i] = start + i;
}
void ProgramModel::placeArrayCompounds(Compound *cArray, int &count, int elementType)
{
	count = getSomeValue(counts[elementType]);
	int j = 0;
	for(int i = 0; i < count; i++)
	{
		Compound *pArray = placeCompound(getSomeValue(sizes[elementType]));
		if(pArray == NULL) continue;
		cArray[j++] = *pArray;
	}
	count = j;
}
ProgramModel::Function::Function(ProgramModel *programm, int level)
{
	programm->placeBytes(variables, varsCount, _VAR);
	programm->placeArrayCompounds(arrays, arraysCount, _ARRAY);
	programm->placeArrayCompounds(structures, structuresCount, _OBJECT);
	programm->placeBytes(code, codeSize, _COMMAND);
	functionsCount = getSomeValue(_C_FUNCTIONS)*1./(1 + level);
	for(int i = 0; i < functionsCount; i++)
		functions[i] = new Function(programm, level+1);
	ciclesCount = getSomeValue(_C_ARRAYS);
	int start = code[0];
	for(int i = 0; i < ciclesCount; i++)
	{
		start += rand()%codeSize;
		int count = getSomeValue(_S_CIRCLE);
		if(start - code[0] + count >= codeSize) {ciclesCount = i; break;}
		cicles[i].countBytes = count;
		cicles[i].start = start;
		start += count+1;
	}
}
int ProgramModel::reserveBytes(int count)
{
	return controller->reserveBytes(count);
}

ProgramModel::~ProgramModel(void)
{
		free(main);
}