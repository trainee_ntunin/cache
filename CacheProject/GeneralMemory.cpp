#include "stdafx.h"
#include "GeneralMemory.h"


GeneralMemory::GeneralMemory(void)
{
	for(int i = 0; i < MEMORY_SIZE; i++)
		memory[i] = 'a' + (char)(i%52);
}

int GeneralMemory::getSize()
{
	return 0;
}

char GeneralMemory::getData(int adress)
{
	if(adress < 0) return adress;
	return memory[adress];
}

void GeneralMemory::setData(int adress, char data)
{
	memory[adress] = data;
}

int GeneralMemory::reserveBytes(int count)
{
	if(count < 1) return -1;
	int start = reserved.empty()? 0 : reserved.top()+1;
	int last = start + count;
	if(last > MEMORY_SIZE) throw MemoryException();
	for(int i = start; i < last; i++)
		reserved.push(i);
	return start;
}

GeneralMemory::~GeneralMemory(void)
{
}
