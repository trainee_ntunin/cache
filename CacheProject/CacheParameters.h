#pragma once
#define _COUNT_VARIANTES 6
//defenitions
#define _CIRCLE 0
#define _VAR 1
#define _COMMAND 2
#define _ARRAY 3
#define _OBJECT 4
#define _FUNCTION 5

//sizes
#define _S_CIRCLE 10
#define _S_VAR 1
#define _S_COMMAND 1
#define _S_ARRAY 100
#define _S_OBJECT 8
#define _S_FUNCTION 70
//counts
#define _C_CIRCLES 2
#define _C_VARS 10
#define _C_COMMANDS 25
#define _C_ARRAYS 1
#define _C_OBJECTS 2
#define _C_FUNCTIONS 3
//possibilityes
#define _P_CIRCLE (100/_S_FUNCTION)*_C_CIRCLES
#define _P_VAR (100/_S_FUNCTION) *_C_VARS
#define _P_COMMAND (100/_S_FUNCTION) * _C_COMMANDS
#define _P_ARRAY (100/_S_FUNCTION) * _C_ARRAYS
#define _P_OBJECT (100/_S_FUNCTION) *_C_OBJECTS
#define _P_FUNCTION (100/_S_FUNCTION) *_C_FUNCTIONS
//fluctuation
#define _FLUCT 0

static const int possibilities[_COUNT_VARIANTES] = {_P_CIRCLE, _P_VAR, _P_COMMAND, _P_ARRAY, _P_OBJECT, _P_FUNCTION};
static const int sizes[_COUNT_VARIANTES] = {_S_CIRCLE, _S_VAR, _S_COMMAND, _S_ARRAY, _S_OBJECT, _S_FUNCTION};
static const int counts[_COUNT_VARIANTES] = {_C_CIRCLES, _C_VARS, _C_COMMANDS, _C_ARRAYS, _C_OBJECTS, _C_FUNCTIONS};

