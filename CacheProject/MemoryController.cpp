#include "stdafx.h"
#include "MemoryController.h"
#include "CacheParameters.h"

DWORD WINAPI StartOperate(LPVOID lParam)
{
	MemoryController *controller = (MemoryController *)lParam;
	controller->Operate();
	return 0;
}
MemoryController::MemoryController(Cache *cache, GeneralMemory *memory)
{
	this->cache = cache;
	this->memory = memory;
	this->requestsCount = 1;
	handleOfOperate = CreateThread(NULL, 0, 
		StartOperate, this, 0, NULL);
}
int MemoryController::reserveBytes(int count)
{
	return memory->reserveBytes(count);
}
void MemoryController::spaceLocality(int adress)
{
	int recordsCount = sizes[getVariant()];
	for(int i = 1; i <= recordsCount; i++)
	{
		cache->noncountedPush(adress + i, memory);
	}
}
char MemoryController::getByte(int adress)
{
	requestsCount++;
	spaceLocality(adress);
	return cache->readAccess(adress, memory);
}

void MemoryController::setByte(int adress, char value)
{
	requestsCount++;
	cache->writeAccess(adress, value, memory);
	spaceLocality(adress);
}

void MemoryController::Operate()
{	
	runed = true;
	while (runed)
	{
		Sleep(50);
		cache->refreshRBit();
	}
}
double MemoryController::getStatistics()
{
	return (1 - (double)cache->getMissCount()/requestsCount);
}
void MemoryController::Stop()
{
	runed = false;
}
MemoryController::~MemoryController(void)
{
	runed = false;
}
