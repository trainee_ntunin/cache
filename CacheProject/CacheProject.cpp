// CacheProject.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "GeneralMemory.h"
#include "Cache.h"
#include "MemoryController.h"
#include "ProgramModel.h"
#include "MemoryException.h"

int _tmain(int argc, _TCHAR* argv[])
{
	Cache cache;
	GeneralMemory memory;
	MemoryController controller(&cache, &memory);
	ProgramModel *programm = NULL;
	try{
	programm = new ProgramModel(&controller);
	} catch(MemoryException ex)
	{
		cout<<"out of memory\n";
	}

	controller.Stop();
	system("pause");
	return 0;
}

