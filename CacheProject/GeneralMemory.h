#pragma once
#include "stdafx.h"
#include "MemoryException.h"
class GeneralMemory
{
	int size;
	char memory[MEMORY_SIZE];
	stack<int> reserved;
public:
	GeneralMemory(void);
	int getSize();
	char getData(int adress);
	void setData(int adress, char data);
	int reserveBytes(int count);
	~GeneralMemory(void);
};

