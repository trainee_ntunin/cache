#pragma once
#include "Cache.h"
#include "GeneralMemory.h"
#include "stdafx.h"

class MemoryController
{
	Cache *cache;
	GeneralMemory *memory;
	bool runed;
	HANDLE handleOfOperate;
	int requestsCount;
	void spaceLocality(int adress);
public:
	MemoryController(Cache *cache, GeneralMemory *memory);
	char getByte(int adress);
	void setByte(int adress, char value);
	int reserveBytes(int count);
	void Operate();
	void Stop();
	double getStatistics();
	~MemoryController(void);
};

