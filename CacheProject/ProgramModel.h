#pragma once
#include "CacheParameters.h"
#include "MemoryController.h"
class ProgramModel
{
private: 
	struct Compound
	{
		int start;
		int countBytes;
	};
	class Function
	{
		int varsCount;
		int ciclesCount;
		int structuresCount;
		int arraysCount;
		int codeSize;
		int functionsCount;

		int variables[2*_C_VARS];
		int code[2*_C_COMMANDS];
		Compound cicles[2*_C_CIRCLES];
		Compound structures[2*_C_OBJECTS];
		Compound arrays[2*_C_ARRAYS];
		Function *functions[2*_C_FUNCTIONS];

	public:
		Function(ProgramModel *programm);
		Function(ProgramModel *programm, int level);
		void execute(MemoryController *controller);
		void execute(int start, int count, MemoryController *controller);
		~Function();
	};

	MemoryController *controller;

	//int functionsCount;
	Function *main;//*functions[2*_C_FUNCTIONS];
	void placeBytes(int *bytes, int &bytesCount, int elementsType);
	void placeArrayCompounds(Compound *cArray, int &size, int elementType);
	Compound *placeCompound(int size);
	int reserveBytes(int count);

	int running;
public:
	ProgramModel(MemoryController *controller);
	void execute();
	void stop();
	~ProgramModel(void);

};

